requirejs.config({
    //By default load any module IDs from js/lib
    baseUrl: 'js', 
    //except, if the module ID starts with "app",
    //load it from the js/app directory. paths
    //config is relative to the baseUrl, and
    //never includes a ".js" extension since
    //the paths config could be for a directory.
    paths: { 
        jquery:'https://cdn.bootcss.com/jquery/3.1.1/jquery.min',
        vue:'https://cdn.bootcss.com/vue/2.1.4/vue',
        bootstrap:'https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min',
        router:'https://cdn.bootcss.com/vue-router/2.7.0/vue-router',
        vuex:'https://cdn.bootcss.com/vuex/2.4.0/vuex',
       'vue-com':'app/vue-com' ,
       'foot-router':'app/foot' ,
       'bar-router':'app/router-test',
       store:'app/store'
    },
    shim:{
        //deps,exports ;  deps 为数组,表示其依赖的库, exports 表示输出的对象名
        //test:{ deps:['jquery'],exports:"test" }
    }
});

