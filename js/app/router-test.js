
define(['router', 'vue'], function (vueRouter, vue) {


  var Bar = { template: '<div>bar</div>' };

  var routes = [
    {
      path: '/bar',
      component: Bar
    }
  ]
 
  return routes;

});