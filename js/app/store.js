
define(['vue','vuex'], function (vue,vuex) {
    
    vue.use(vuex);

    var store = new vuex.Store({
        state: {
          count: 0,
          message:""
        },
        mutations: {
          increment (state) {
            state.count++
          },
          updateMessage (state, message) {
            state.message = message
          }
        }
      });
 

    return store;
});