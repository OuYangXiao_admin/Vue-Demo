define(['router', 'vue'], function (vueRouter, vue) {
    var Foo = {
        data: function () {
            return {
                vName: "222"
            };
        },
        created: function () {
            console.log(" getInfo created ");
            this.getInfo();
        },
        methods: {
            getInfo: function () {
                this.vName = "get info ";
            }
        },
        template: '<div >foo - {{vName}}</div>'
    };

    var routes = [{
        path: '/foo',
        component: Foo
    }]
    return routes;
});