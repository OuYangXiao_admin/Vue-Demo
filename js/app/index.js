// index 页面的操作处理逻辑
//

define(['vue-com', 'router','store', 'vuex', 'bar-router', 'foot-router'], function (vue,vueRouter, store, vuex, bar, foot) {
 
    //挂载router插件
    vue.use(vueRouter);

    //创建路由实例
    var router = new vueRouter({
        routes : bar.concat(foot)
    });

    // 注册模块 `myModule`
    // store.registerModule('myModule', {
    //     // ...
    // })
    // // 注册嵌套模块 `nested/myModule`
    // store.registerModule(['nested', 'myModule'], {
    //     // ...
    // })

    var app = new vue({
        el: '#app',
        router: router,//注入router实例
        store: store,//租入store实例
        data: {
            title: '一个测试',
            rTitle: '路由头', 
            getList: [{
                    text: "11111"
                },
                {
                    text: "22222"
                },
                {
                    text: "33333"
                },
                {
                    text: "44444"
                },
            ]
        },
        methods: {
            add: function () {
                this.count++;
            },
        },
        computed: {
              /* 方法1： 把store的属性映射 组件中*/
            message: {
                get() {
                    return this.$store.state.message;
                },
                set(value) {
                    //这种方式有好处，可以统计规范 message 的值。
                    this.$store.commit('updateMessage', value)
                },
                 
            },
            /* 方法2：把store的属性映射 组件中*/
            count:{
                get() {
                    return this.$store.state.count;
                },
                set(value) {
                      //这种方式虽然简单，但是如果多个组件都这么写，会出现找不到到底是哪个组件更新的情况。难调试
                     this.$store.state.count = value;
                },
              
            }
        }

    });

    console.log(store.state.count);

    return app;

});



// const moduleA = {
//     state: { count: 0 },
//     mutations: {
//         increment(state) {
//             // 这里的 `state` 对象是模块的局部状态
//             state.count++
//         }
//     },

//     getters: {
//         doubleCount(state) {
//             return state.count * 2
//         }
//     },
//     actions: {

//     }
// }


// Vue.component("test", {
//     props: ['comValue'],
//     data: function () {

//     },
//     created: function () {
//         console.log(this.comValue);
//         alert(this.comValue);
//     },
//     template: '<h1>{{this.comValue}}</h1>'
// });

// Vue.component("mycontent", {
//     props: ['contents'],
//     data: {
//         coms: []
//     },
//     render: function (h) {
//         this.coms = [];
//         for (var i = 0; i < this.contents.length; i++) {
//             this.coms.push(h(this.contents[i].name, { props: this.contents[i].props }));
//         }
//         return h('div', {}, this.coms);
//     }
// })

// new Vue({
//     el: '.admin-content-body',
//     data: {
//         list: [],
//         info: {},
//         contents: [{ name: "test", props: { 'comValue': 'testvalue' } }]
//     },
//     created: function () {

//     },
//     computed: {

//     },
//     methods: {
//         add: function () {
//             this.info = {};

//         },
//         jump: function () {
//             $("form.form-search").jump();
//             loading("加载中");
//         }
//     }
// });