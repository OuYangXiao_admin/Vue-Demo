//Vue 一些通用的组件

define(['vue'], function (vue) {
    
    vue.component('todo-item', {
            props: ['todo'],
            template: '<li>{{ todo.text }}</li>'
            
        });

    return vue;
});